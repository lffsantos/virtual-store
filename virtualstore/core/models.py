from django.db import models


class Customer(models.Model):
    name = models.CharField('nome', max_length=200, null=False, blank=False)
    email = models.EmailField(null=False, blank=False)
    created_at = models.DateTimeField('criado em', auto_now_add=True)
    updated_at = models.DateTimeField('atualizado em', auto_now=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField('nome', max_length=50, null=False, blank=False)
    description = models.CharField('nome', max_length=200, null=False, blank=False)
    value = models.DecimalField('valor', max_digits=17, decimal_places=2)
    created_at = models.DateTimeField('criado em', auto_now_add=True)
    updated_at = models.DateTimeField('atualizado em', auto_now=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class Ordered(models.Model):
    customer = models.ForeignKey("Customer", on_delete=models.PROTECT, related_name='orders')
    note = models.CharField(max_length=300, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    value = models.FloatField(blank=True, null=False, default=0)
    products = models.ManyToManyField(Product)

    class Meta:
        ordering = ('created_at',)

    def __str__(self):
        return self.client.name
