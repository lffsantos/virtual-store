from django.urls import include, path
from rest_framework import routers

from virtualstore.core.views import CustomerViewSet, OrderedViewSet, ProductViewSet

app_name = 'core'


router = routers.DefaultRouter()
router.register(r'customers', CustomerViewSet, 'customers')
router.register(r'products', ProductViewSet)
router.register(r'orders', OrderedViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
