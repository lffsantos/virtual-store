from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.status import HTTP_404_NOT_FOUND

from virtualstore.core.models import Product, Customer, Ordered
from virtualstore.core.serializers import ProductSerializer, CustomerSerializer, OrderedSerializer


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    filterset_fields = ['name', 'email']

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            if instance.orders.all():
                return Response("Can not delete customers with orders")

            self.perform_destroy(instance)
        except HTTP_404_NOT_FOUND:
            return Response(status=status.HTTP_404_NOT_FOUND)

        return Response(status=status.HTTP_204_NO_CONTENT)


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            if instance.ordered_set.all():
                return Response("Can not delete products with orders")

            self.perform_destroy(instance)
        except HTTP_404_NOT_FOUND:
            return Response(status=status.HTTP_404_NOT_FOUND)

        return Response(status=status.HTTP_204_NO_CONTENT)


class OrderedViewSet(viewsets.ModelViewSet):
    http_method_names = ['get', 'post', 'delete', 'patch']
    queryset = Ordered.objects.all()
    serializer_class = OrderedSerializer
    filterset_fields = ['customer', 'products']

