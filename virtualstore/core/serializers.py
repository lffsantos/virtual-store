from rest_framework import serializers

from virtualstore.core.models import Product, Customer, Ordered


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class OrderedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ordered
        fields = '__all__'

    def create(self, validated_data):
        products = validated_data.pop('products')
        value = sum([p.value for p in products])
        ordered = Ordered.objects.create(**validated_data, value=value)
        ordered.products.set(products)
        return ordered
