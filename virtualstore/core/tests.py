from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from model_bakery import baker

from virtualstore.core.models import Customer, Product, Ordered


class CustomerViewTest(TestCase):

    def setUp(self):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        user = baker.make(User)
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        self.token = f'JWT {token}'

    def test_create_customer(self):
        """
        Ensure we can create a new customer object.
        """
        data = {
          "name": "teste",
          "email": "teste@gmail.com",
        }
        response = self.client.post('/customers/', data, HTTP_AUTHORIZATION=self.token, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Customer.objects.count(), 1)
        self.assertEqual(Customer.objects.get().name, data['name'])
#

    def test_put_customer(self):
        """
        Ensure we can create a update customer object.
        """
        data = {
            "name": "teste edit",
            "email": "teste@gmail.com",
        }
        customer = Customer.objects.create(**data)
        data['name'] = "teste 2"
        response = self.client.put(
            '/customers/{}/'.format(str(customer.id)), data, HTTP_AUTHORIZATION=self.token,
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Customer.objects.count(), 1)
        self.assertEqual(Customer.objects.get().name, data['name'])

    def test_patch_customer(self):
        """
        Ensure we can create a update partial customer object.
        """
        data = {
            "name": "teste edit",
            "email": "teste@gmail.com",
        }
        customer = Customer.objects.create(**data)
        data_2 = {"name": "new name"}
        response = self.client.patch(
            '/customers/{}/'.format(str(customer.id)), data_2, HTTP_AUTHORIZATION=self.token,
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Customer.objects.count(), 1)
        self.assertEqual(Customer.objects.get().name, data_2['name'])

    def test_delete_customer(self):
        """
        Ensure we can delete a object.
        """
        data = {
            "name": "teste edit",
            "email": "teste@gmail.com",
        }
        customer = Customer.objects.create(**data)
        response = self.client.delete('/customers/{}/'.format(str(customer.id)), HTTP_AUTHORIZATION=self.token)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Customer.objects.count(), 0)

    def test_delete_customer_with_order(self):
        """
        Ensure we can't delete a customer object with order.
        """
        data = {
            "name": "teste edit",
            "email": "teste@gmail.com",
        }
        customer = Customer.objects.create(**data)
        product = Product.objects.create(name="p1", description="product 1", value=10)
        order = Ordered.objects.create(customer=customer,  value=10)
        order.products.set([product])
        response = self.client.delete('/customers/{}/'.format(str(customer.id)), HTTP_AUTHORIZATION=self.token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Customer.objects.count(), 1)


class ProductViewTest(TestCase):

    def setUp(self):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        user = baker.make(User)
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        self.token = f'JWT {token}'

    def test_create_product(self):
        """
        Ensure we can create a new product object.
        """
        data = {
          "name": "teste",
          "description": "teste x",
          "value": 10
        }
        response = self.client.post('/products/', data, HTTP_AUTHORIZATION=self.token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Product.objects.count(), 1)
        self.assertEqual(Product.objects.get().name, data['name'])

    def test_put_product(self):
        """
        Ensure we can create a update product object.
        """
        data = {
            "name": "teste",
            "description": "teste x",
            "value": 10
        }
        customer = Product.objects.create(**data)
        data['name'] = "teste 2"
        response = self.client.put(
            '/products/{}/'.format(str(customer.id)), data, HTTP_AUTHORIZATION=self.token,
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Product.objects.count(), 1)
        self.assertEqual(Product.objects.get().name, data['name'])

    def test_patch_product(self):
        """
        Ensure we can create a update partial product object.
        """
        data = {
            "name": "teste",
            "description": "teste x",
            "value": 10
        }
        product = Product.objects.create(**data)
        data_2 = {"name": "new name"}
        response = self.client.patch(
            '/products/{}/'.format(str(product.id)), data_2, HTTP_AUTHORIZATION=self.token,
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Product.objects.count(), 1)
        self.assertEqual(Product.objects.get().name, data_2['name'])

    def test_delete_product(self):
        """
        Ensure we can delete a object.
        """
        data = {
            "name": "teste",
            "description": "teste x",
            "value": 10
        }
        product = Product.objects.create(**data)
        response = self.client.delete('/products/{}/'.format(str(product.id)), HTTP_AUTHORIZATION=self.token)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Product.objects.count(), 0)

    def test_delete_product_with_order(self):
        """
        Ensure we can't delete a product object with order.
        """
        data = {
            "name": "teste edit",
            "email": "teste@gmail.com",
        }
        customer = Customer.objects.create(**data)
        product = Product.objects.create(name="p1", description="product 1", value=10)
        order = Ordered.objects.create(customer=customer,  value=10)
        order.products.set([product])
        response = self.client.delete('/products/{}/'.format(str(product.id)), HTTP_AUTHORIZATION=self.token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Product.objects.count(), 1)


class OrderedViewTest(TestCase):

    def setUp(self):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        user = baker.make(User)
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        self.token = f'JWT {token}'

        data = {
            "name": "teste edit",
            "email": "teste@gmail.com",
        }

        self.customer = Customer.objects.create(**data)
        self.product = Product.objects.create(name="p1", description="product 1", value=10)
        self.product2 = Product.objects.create(name="p2", description="product 2", value=13)

    def test_create_order(self):
        """
        Ensure we can create a new order object.
        """
        data = {
          "note": "order 1",
          "customer": self.customer.id,
          "products": [
            str(self.product.id)
          ]
        }
        response = self.client.post('/orders/', data, HTTP_AUTHORIZATION=self.token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Ordered.objects.count(), 1)
        self.assertEqual(Ordered.objects.get().products.get(), self.product)

    def test_patch_order(self):
        """
        Ensure we can create a update partial order object.
        """
        data = {
            "note": "order 1",
            "customer": self.customer
        }

        order = Ordered.objects.create(**data)
        order.products.set([str(self.product.id)])

        data_2 = {
            "note": "order change"
        }
        response = self.client.patch(
            '/orders/{}/'.format(str(order.id)), data_2, HTTP_AUTHORIZATION=self.token,
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Ordered.objects.count(), 1)
        self.assertEqual(Ordered.objects.get().note, data_2['note'])

    def test_delete_order(self):
        """
        Ensure we can delete a object.
        """
        data = {
            "note": "order 1",
            "customer": self.customer
        }

        order = Ordered.objects.create(**data)
        order.products.set([str(self.product.id)])
        response = self.client.delete('/orders/{}/'.format(str(order.id)), HTTP_AUTHORIZATION=self.token)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Ordered.objects.count(), 0)

