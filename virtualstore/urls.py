from django.contrib import admin
from django.urls import include, path
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='VirtualStore API')
#
urlpatterns = [
    path('', include('virtualstore.core.urls', namespace='core')),
    path('admin/', admin.site.urls),
    path('docs/', schema_view, name="docs"),
    path('login/', obtain_jwt_token),
    path('refresh-token/', refresh_jwt_token),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
